��    
      l      �       �      �   3     U   A  #   �  ,   �  �   �  �   k  r   �  H   `  �  �     ,  A   G  e   �  %   �  )     z   ?  }   �  �   8  ;   �                                   	   
       Connecting to ${network}... Enter text and press enter. Press escape to cancel. Enter the password for ${network}. Just press the enter key if $network is unsecured. Please select your wireless network Scanning for available wireless networks ... There was a problem configuring the wireless network. Be sure you selected the correct network, and that your password is correct. Unable to get a list of networks. Either you have no wireless networks in range or there is a problem with your wireless adapter. Use the up and down arrow keys to find the option you want, then press enter to select it. Press escape to cancel. Your wireless network connection has been configured and is now working. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-06-19 23:00-0400
PO-Revision-Date: 2018-07-05 22:11-0400
Last-Translator: Roberta Santos <robertasantos1997@gmail.com>
Language-Team: Brazilian Portuguese
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Conectando a ${network}... Digite o texto e pressione enter. Pressione escape para cancelar. Digite a senha para ${network}. Basta pressionar a tecla enter se a rede $network não possuir senha. Por favor, selecione sua rede sem fio Procurando redes sem fio disponíveis ... Houve um problema ao configurar a rede sem fio. Verifique se você selecionou a rede correta e se sua senha está correta. Não é possível obter uma lista de redes. Não existem redes sem fio ao redor ou há um problema com seu adaptador sem fio. Use as teclas de seta para cima e para baixo para encontrar a opção desejada e pressione enter para selecionar. Pressione escape para cancelar. Sua rede sem fio foi configurada e agora está funcionando. 